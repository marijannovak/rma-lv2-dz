package com.marijannovak.zadacalv2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ResultActivity extends Activity {

    private static final String KEY_UNIT1 = "key_unit1";
    private static final String KEY_UNIT2 = "key_unit2";

    TextView unit1Text, unit2Text, equalsText;
    Button bBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "my_font.otf");

        unit1Text = (TextView) findViewById(R.id.unit1);
        unit2Text = (TextView) findViewById(R.id.unit2);
        equalsText = (TextView) findViewById(R.id.isText);

        unit1Text.setTypeface(myTypeface);
        unit2Text.setTypeface(myTypeface);
        equalsText.setTypeface(myTypeface);

        bBack = (Button) findViewById(R.id.bBack);

        Intent startingIntent = this.getIntent();

        if(startingIntent.hasExtra(KEY_UNIT1))
        {
            unit1Text.setText(startingIntent.getStringExtra(KEY_UNIT1));
        }

        if(startingIntent.hasExtra(KEY_UNIT2))
        {
            unit2Text.setText(startingIntent.getStringExtra(KEY_UNIT2));
        }

        bBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(backIntent);
                finish();
            }
        });

    }

}
