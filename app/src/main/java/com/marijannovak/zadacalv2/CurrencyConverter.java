package com.marijannovak.zadacalv2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class CurrencyConverter extends Activity {

    private static final String KEY_UNIT1 = "key_unit1";
    private static final String KEY_UNIT2 = "key_unit2";

    Spinner spinner1, spinner2;
    EditText inputText;
    Button bConvert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_converter);

        spinner1 = (Spinner) findViewById(R.id.spinnerCurrency);
        spinner2 = (Spinner) findViewById(R.id.spinner2Currency);
        inputText = (EditText) findViewById(R.id.editTextCurrency);
        bConvert = (Button) findViewById(R.id.currencyConvert);



        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array. currency_units, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        spinner2.setAdapter(adapter);


        spinner2.setSelection(1);

        bConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(TextUtils.isEmpty(inputText.getText().toString()))
                {
                    inputText.setError("You must enter a number!");
                }

                else
                {
                convert(Double.parseDouble(inputText.getText().toString()), getUnit1(), getUnit2());
                }
            }
        });
    }

    private void convert(double input, int unit1, int unit2) {

        double result = 0;

        switch (unit1)
        {
            case 0:
                if (unit2  == 0) result = input;
                else if (unit2  == 1) result = input*0.937954322;
                else if (unit2  == 2) result = input*6.98655786;
                else if (unit2  == 3) result = input*0.797352789;

                break;

            case 1:

                if (unit2  == 0) result = input*1.06615;
                else if (unit2  == 1) result = input;
                else if (unit2  == 2) result = input*7.44871867;
                else if (unit2  == 3) result = input*0.850097676;
                break;

            case 2:

                if (unit2  == 0) result = input*0.143132 ;
                else if (unit2  == 1) result = input*0.134251278;
                else if (unit2  == 2) result = input;
                else if (unit2  == 3) result = input*0.114126699;
                break;

            case 3:

                if (unit2  == 0) result = input* 1.25415;
                else if (unit2  == 1) result = input*1.17633541;
                else if (unit2  == 2) result = input* 8.76219154;
                else if (unit2  == 3) result = input;
                break;
        }

        String unit1String = String.format("%.2f", Double.parseDouble(inputText.getText().toString()));
        String unit2String = String.format("%.2f", result);

        Intent resultIntent = new Intent(this, ResultActivity.class);
        resultIntent.putExtra(KEY_UNIT1, unit1String + " "+ spinner1.getSelectedItem().toString());
        resultIntent.putExtra(KEY_UNIT2, unit2String + " " + spinner2.getSelectedItem().toString());

        startActivity(resultIntent);
    }


    public int getUnit1() {
        return spinner1.getSelectedItemPosition();
    }

    public int getUnit2() {
        return spinner2.getSelectedItemPosition();
    }
    }

