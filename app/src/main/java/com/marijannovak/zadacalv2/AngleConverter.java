package com.marijannovak.zadacalv2;

import android.app.Activity;
import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class AngleConverter extends Activity {

    private static final String KEY_UNIT1 = "key_unit1";
    private static final String KEY_UNIT2 = "key_unit2";
    Spinner spinner1, spinner2;
    EditText inputText;
    Button bConvert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_angle_converter);


        spinner1 = (Spinner) findViewById(R.id.spinnerAngle);
        spinner2 = (Spinner) findViewById(R.id.spinner2Angle);
        inputText = (EditText) findViewById(R.id.editTextAngle);
        bConvert = (Button) findViewById(R.id.angleConvert);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.angle_units, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        spinner2.setAdapter(adapter);

        spinner2.setSelection(1);

        bConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(inputText.getText().toString()))
                {
                    inputText.setError("You must enter a number!");
                }

                else
                {
                    convert(Double.parseDouble(inputText.getText().toString()), getUnit1(), getUnit2());
                }
            }
        });
    }

    private void convert(double input, int unit1, int unit2) {

        double result = 0;

        switch (unit1)
        {
            case 0:
                if (unit2  == 0) result = input;
                else if (unit2  == 1) result = input*0.0174532925;
                else if (unit2  == 2) result = input*1.1111111111111112;
                else if (unit2  == 3) result = input*0.71111111;

                break;

            case 1:

                if (unit2  == 0) result = input*57.2957795;
                else if (unit2  == 1) result = input;
                else if (unit2  == 2) result = input*63.66197723676;
                else if (unit2  == 3) result = input*40.74366543;
                break;

            case 2:

                if (unit2  == 0) result = input*0.9;
                else if (unit2  == 1) result = input*0.015707963267949;
                else if (unit2  == 2) result = input;
                else if (unit2  == 3) result = input*0.64;
                break;

            case 3:

                if (unit2  == 0) result = input* 1.40625;
                else if (unit2  == 1) result = input*0.02454369;
                else if (unit2  == 2) result = input* 1.5625;
                else if (unit2  == 3) result = input;
                break;
        }

        String unit1String = String.format("%.2f", Double.parseDouble(inputText.getText().toString()));
        String unit2String = String.format("%.2f", result);

        Intent resultIntent = new Intent(this, ResultActivity.class);
        resultIntent.putExtra(KEY_UNIT1, unit1String + " "+ spinner1.getSelectedItem().toString());
        resultIntent.putExtra(KEY_UNIT2, unit2String + " " + spinner2.getSelectedItem().toString());

        startActivity(resultIntent);
    }


    public int getUnit1() {
        return spinner1.getSelectedItemPosition();
    }

    public int getUnit2() {
        return spinner2.getSelectedItemPosition();
    }
}
