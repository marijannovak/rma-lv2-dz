package com.marijannovak.zadacalv2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class LengthConverter extends Activity {


    private static final String KEY_UNIT1 = "key_unit1";
    private static final String KEY_UNIT2 = "key_unit2";
    Spinner spinner1, spinner2;
    EditText inputText;
    Button bConvert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_length_converter);

        spinner1 = (Spinner) findViewById(R.id.spinnerLength);
        spinner2 = (Spinner) findViewById(R.id.spinner2Length);
        inputText = (EditText) findViewById(R.id.editTextLength);
        bConvert = (Button) findViewById(R.id.lengthConvert);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array. length_units, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        spinner2.setAdapter(adapter);

        spinner2.setSelection(1);

        bConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(inputText.getText().toString()))
                {
                    inputText.setError("You must enter a number!");
                }

                else
                {
                    convert(Double.parseDouble(inputText.getText().toString()), getUnit1(), getUnit2());
                }
            }
        });

    }

    private void convert(double input, int unit1, int unit2) {

        double result = 0;

        switch (unit1)
        {
            case 0:
                if (unit2  == 0) result = input;
                else if (unit2  == 1) result = input*3.2808399;
                else if (unit2  == 2) result = input*0.000621371192;
                else if (unit2  == 3) result = input*0.001;

                break;

            case 1:

                if (unit2  == 0) result = input*0.3048;
                else if (unit2  == 1) result = input;
                else if (unit2  == 2) result = input*0.000189393939;
                else if (unit2  == 3) result = input*0.0003048;
                break;

            case 2:

                if (unit2  == 0) result = input*1609.344;
                else if (unit2  == 1) result = input*5280;
                else if (unit2  == 2) result = input;
                else if (unit2  == 3) result = input*1.609344;
                break;

            case 3:

                if (unit2  == 0) result = input* 1000;
                else if (unit2  == 1) result = input*3280.8399;
                else if (unit2  == 2) result = input*0.621371192;
                else if (unit2  == 3) result = input;
                break;
        }

        String unit1String = String.format("%.5f", Double.parseDouble(inputText.getText().toString()));
        String unit2String = String.format("%.5f", result);

        Intent resultIntent = new Intent(this, ResultActivity.class);
        resultIntent.putExtra(KEY_UNIT1, unit1String + " "+ spinner1.getSelectedItem().toString());
        resultIntent.putExtra(KEY_UNIT2, unit2String + " " + spinner2.getSelectedItem().toString());

        startActivity(resultIntent);
    }


    public int getUnit1() {
        return spinner1.getSelectedItemPosition();
    }

    public int getUnit2() {
        return spinner2.getSelectedItemPosition();
    }
}
