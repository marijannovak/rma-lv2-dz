package com.marijannovak.zadacalv2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class VolumeConverter extends Activity {

    private static final String KEY_UNIT1 = "key_unit1";
    private static final String KEY_UNIT2 = "key_unit2";
    Spinner spinner1, spinner2;
    EditText inputText;
    Button bConvert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volume_converter);


        spinner1 = (Spinner) findViewById(R.id.spinnerVolume);
        spinner2 = (Spinner) findViewById(R.id.spinner2Volume);
        inputText = (EditText) findViewById(R.id.editTextVolume);
        bConvert = (Button) findViewById(R.id.volumeConvert);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.volume_units, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        spinner2.setAdapter(adapter);


        spinner2.setSelection(1);

        bConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(inputText.getText().toString()))
                {
                    inputText.setError("You must enter a number!");
                }

                else
                {
                    convert(Double.parseDouble(inputText.getText().toString()), getUnit1(), getUnit2());
                }
            }
        });
    }

    private void convert(double input, int unit1, int unit2) {

        double result = 0;

        switch (unit1)
        {
            case 0:
                if (unit2  == 0) result = input;
                else if (unit2  == 1) result = input*1000;
                else if (unit2  == 2) result = input*264.172052;
                else if (unit2  == 3) result = input*35.3146667;

                break;

            case 1:

                if (unit2  == 0) result = input*0.001;
                else if (unit2  == 1) result = input;
                else if (unit2  == 2) result = input*0.264172052;
                else if (unit2  == 3) result = input*0.0353146667;
                break;

            case 2:

                if (unit2  == 0) result = input*0.00378541178;
                else if (unit2  == 1) result = input*3.78541178;
                else if (unit2  == 2) result = input;
                else if (unit2  == 3) result = input*0.133680556;
                break;

            case 3:

                if (unit2  == 0) result = input* 0.0283168466;
                else if (unit2  == 1) result = input*28.3168466;
                else if (unit2  == 2) result = input* 7.48051948;
                else if (unit2  == 3) result = input;
                break;
        }

        String unit1String = String.format("%.4f", Double.parseDouble(inputText.getText().toString()));
        String unit2String = String.format("%.4f", result);

        Intent resultIntent = new Intent(this, ResultActivity.class);
        resultIntent.putExtra(KEY_UNIT1, unit1String + " "+ spinner1.getSelectedItem().toString());
        resultIntent.putExtra(KEY_UNIT2, unit2String + " " + spinner2.getSelectedItem().toString());

        startActivity(resultIntent);
    }


    public int getUnit1() {
        return spinner1.getSelectedItemPosition();
    }

    public int getUnit2() {
        return spinner2.getSelectedItemPosition();
    }

    }

