package com.marijannovak.zadacalv2;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends Activity implements View.OnClickListener{

    ImageButton angle, volume, currency, length;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        angle = (ImageButton) findViewById(R.id.angleImageButton);
        volume = (ImageButton) findViewById(R.id.volumeImageButton);
        currency = (ImageButton) findViewById(R.id.currencyImageButton);
        length = (ImageButton) findViewById(R.id.lengthImageButton);

        angle.setOnClickListener(this);
        volume.setOnClickListener(this);
        currency.setOnClickListener(this);
        length.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.angleImageButton:
                convertAngle();
                break;

            case R.id.volumeImageButton:
                convertVolume();
                break;

            case R.id.currencyImageButton:
                convertCurrency();
                break;

            case R.id.lengthImageButton:
                convertLength();
                break;
        }
    }

    private void convertCurrency() {
        Intent currencyIntent = new Intent(this, CurrencyConverter.class);
        startActivity(currencyIntent);

    }

    private void convertVolume() {

        Intent volumeIntent = new Intent(this, VolumeConverter.class);
        startActivity(volumeIntent);

    }

    private void convertLength() {

        Intent lengthIntent = new Intent(this, LengthConverter.class);
        startActivity(lengthIntent);

    }

    private void convertAngle() {
        Intent angleIntent = new Intent(this, AngleConverter.class);
        startActivity(angleIntent);

    }
}
