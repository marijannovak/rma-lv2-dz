Ova aplikacija jest implementacija konvertera volumena, duljina, valute i kuta. Svaki od tih ima zaseban Activity koji intentom poziva Activity za prikaz rezultata te mu predaje upisanu vrijednost i pretvoreni rezultat zajedno s njihovim mjernim jedinicama u obliku stringa. Na pocetnom ekranu se bira sto konvertiramo, pa se u jednom od konvertera u EditText (numeric decimal) upisuje vrijednost, a na spinnerima odabiru mjerne jedinice. Pritiskom na gumb "Convert" racuna se vrijednost te se ona šalje na ResultActivity i prikazuje rezultat. Za prikaz rezultata koristi se "custom" typeface, tj. font. Na aktivnosti za prikaz rezultata također se nalazi gumb za povratak na početni meni. 

Većih problema nije bilo. Najdulje je trajalo upisivanje koeficijenata kojim se mnozi upisana vrijednost za dobivanje trazene mjerne jedinice.

Screenshotovi se nalaze u mapi 'screenshots'

Aplikaciju moguće jednostavno izvesti u samo 2 activitya.